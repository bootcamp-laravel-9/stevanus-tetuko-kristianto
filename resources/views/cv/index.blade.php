@extends('layout.main')
{{-- section ('('nama yield', 'valuenya')') --}}
@section('menu-title', 'Curriculum Vitae')
@section('menu-cv', 'active')
@section('content')
<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <title>Table</title>
  </head>
  <body>
    <div style="display: flex">
        {{-- Konten Kiri --}}
        <div class="card" style="display: flex-row; width:30%; margin-right:20px">
            <div>
                <div class="card-body box-profile">
                    <div class="text-center">
                        <img src="{{ asset('AdminLTE/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
                    </div>
                    <h3 class="profile-username text-center">Stevanus Tetuko Kristianto </h3>
                    <p class="text-muted text-center">Web Dev & UI/UX Designer <br>
                      <a style="margin-right: 10px" href="https://l.instagram.com/?u=https%3A%2F%2Frb.gy%2Fpie4c2&e=AT2bpokVmNu-Zx4w-AqcMjQqKX30YeHRqe84C7XDoGXo-jzzD_syI9LNxMWwv_41aNGmPtM-dsH-mb0l8tJixKfklNt3cApHfUQ2KQ"><i class="fa-solid fa-link"></i></a>
                      <a style="margin-right: 10px" href="https://dribbble.com/NachtVan"><i class="fa-solid fa-basketball"></i></a>
                      <a style="margin-right: 10px" href="https://www.instagram.com/stevanustetuko/"><i class="fa-brands fa-instagram"></i></i></a>
                      <a style="margin-right: 10px" href="https://www.linkedin.com/in/stevanus-tetuko-kristianto-822217225/"><i class="fa-brands fa-linkedin"></i></a>
                    </p>
                  </div>
            </div>
    
            <div >
                <div class="card-body">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th style="width: 10px">#</th>
                          <th>Skill</th>
                          <th>Level</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>1.</td>
                          <td>HTML</td>
                          <td>
                            <div class="progress progress-xs" style="border-radius: 20px">
                              <div class="progress-bar progress-bar-danger" style="width: 90%"></div>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td>2.</td>
                          <td>CSS</td>
                          <td>
                            <div class="progress progress-xs" style="border-radius: 20px">
                              <div class="progress-bar bg-warning" style="width: 70%"></div>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td>3.</td>
                          <td>PHP</td>
                          <td>
                            <div class="progress progress-xs" style="border-radius: 20px">
                              <div class="progress-bar bg-success" style="width: 70%"></div>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td>4.</td>
                          <td>React</td>
                          <td>
                            <div class="progress progress-xs" style="border-radius: 20px">
                              <div class="progress-bar bg-danger" style="width: 60%"></div>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td>5.</td>
                          <td>MySql</td>
                          <td>
                            <div class="progress progress-xs" style="border-radius: 20px">
                              <div class="progress-bar bg-info" style="width: 75%"></div>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td>6.</td>
                          <td>Figma</td>
                          <td>
                            <div class="progress progress-xs" style="border-radius: 20px">
                              <div class="progress-bar bg-primary" style="width: 80%"></div>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td>7.</td>
                          <td>Unity</td>
                          <td>
                            <div class="progress progress-xs" style="border-radius: 20px">
                              <div class="progress-bar bg-warning" style="width: 50%"></div>
                            </div>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                </div>
            </div>
        </div>
        
        {{-- Konten Kanan --}}
        <div style="width:70%">
            {{-- Tentang Saya --}}
            <div>
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Tentang Saya</h3>
                    </div>
                    <div class="card-body">
                        <p>
                            Saya seorang Sarjana Komputer dari Universitas Atma Jaya Yogyakarta. Saat ini saya sedang mengembangkan diri dengan 
                            mempelajari beberapa hal baru mengenai dunia IT dan UI/UX. Saya saat ini juga sedang mencari pekerjaan untuk 
                            mengembangkan diri dengan pengalaman dan pengetahuan dalam dunia IT dan UI/UX Design. Saya memiliki antusias dan 
                            motivasi tinggi dalam mempelajari hal baru. Saya memiliki pengalaman dalam menciptakan beberapa website dan aplikasi 
                            mobile secara mandiri maupun berkelompok. Saya juga memiliki pengalaman dalam mendesain website dan mobile app.
                        </p>
                    </div>            
                </div>
            </div>
        
            {{-- Pendidikan --}}
            <div>
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Pendidikan</h3>
                    </div>
                    <div class="card-body">
                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                              <b>Universitas Atma Jaya Yogyakarta</b> <p class="float-right">Agu 2019 – Agu 2023</p>
                              <p>S1 Informatika <br> IPK 3.84</p> 
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            {{-- Pengalaman Kerja --}}
            <div>
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Pengalaman Kerja</h3>
                    </div>
                    <div class="card-body">
                        <ul class="list-group list-group-unbordered mb-3">
                            <li class="list-group-item">
                            <b>Magang | PT. Arutala Digital Inovasi</b> <p class="float-right">Jul 2022 - Nov 2022</p>
                            <p>- Mengembangkan sebuah aplikasi VR untuk membantu mengenalkan dan mempelajari gamelan Kenong 
                                menggunakan VR 
                                <br> 
                                - Dibangun bersama tim dan saya berperan dalam pembuatan user persona, flow diagram, mockup, serta berperan 
                                juga sebagai developer dimana saya yang membangun aplikasi VR menggunakan Unity dan Blender</p> 
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            {{-- Organisasi --}}
            <div>
              <div class="card">
                  <div class="card-header">
                      <h3 class="card-title">Organisasi</h3>
                  </div>
                  <div class="card-body">
                      <ul class="list-group list-group-unbordered mb-3">
                          <li class="list-group-item">
                          <b>Anggota | Kelompok Studi Pemrograman</b>
                          <p>Mempelajari bahasa pemrograman C bersama anggota lainnya setiap minggu secara offline</p> 
                          </li>
                          <li class="list-group-item">
                            <b>Anggota | Komunitas E-Sport UAJY</b>
                            <p>Setiap minggu melakukan pertemuan dan bermain game e-sport bersama anggota lainnya secara online</p> 
                          </li>
                      </ul>
                  </div>
              </div>
            </div>

        </div>
    </div>
  </body>
</html>

@endsection