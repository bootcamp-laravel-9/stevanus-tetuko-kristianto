<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Login</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('AdminLTE/plugins/fontawesome-free/css/all.min.css')}}">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{ asset('AdminLTE/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('AdminLTE/dist/css/adminlte.min.css')}}">
</head>
<body class="hold-transition login-page" style="background-image: url('https://images.unsplash.com/photo-1635776062360-af423602aff3?q=80&w=1932&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D')">
    <div class="login-box">
        <div class="login-logo">
            <b>Login</b>
        </div>
        <!-- /.login-logo -->
        <div class="card" style="box-shadow: 10px 10px 10px rgb(0, 0, 0)">
            <div class="card-body login-card-body" style="border-radius: 50px" >
                <p class="login-box-msg">Sign in to start your session</p>

                <form action="../../index3.html" method="post">
                    <div class="input-group mb-3">
                        <input type="email" class="form-control" placeholder="Email">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span><i class="fa-regular fa-envelope"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input type="password" class="form-control" placeholder="Password">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span><i class="fa-solid fa-lock"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-8">
                            <div class="icheck-primary">
                                <input type="checkbox" id="remember">
                                <label for="remember">
                                    Remember Me
                                </label>
                            </div>
                        </div>
                        <!-- /.col -->
                        {{-- <div class="col-4">
                            <button type="submit" class="btn btn-block" style="background-color: rgb(3, 3, 3); color: aliceblue; border-radius: 50px">Login</button>
                        </div> --}}
                        <!-- /.col -->
                    </div>
                </form>
                <div style="margin-top: 10px">
                    <button type="submit" class="btn btn-block" style="background-color: rgb(3, 3, 3); color: aliceblue; border-radius: 50px">Login</button>
                </div>
                <div class="social-auth-links text-center mb-3">
                    <hr style="background-color: grey; height: 1px;">
                    <a href="#" class="btn btn-block" style="background-color: rgb(3, 3, 3); color: aliceblue; border-radius: 50px">
                        <i class="fab fa-google-plus mr-2" ></i> Sign in using Google+
                    </a>
                </div>
                <!-- /.social-auth-links -->

                <p class="mb-1">
                    <a href="forgot-password.html">I forgot my password</a>
                </p>
                <p class="mb-0">
                    <a href="{{ url('/register') }}" class="text-center">Register a new membership</a>
                </p>
            </div>
            <!-- /.login-card-body -->
        </div>
    </div>
<!-- /.login-box -->

<!-- jQuery -->
  <script src="{{ asset('AdminLTE/plugins/jquery/jquery.min.js') }}"></script>
  <!-- Bootstrap 4 -->
  <script src="{{ asset('AdminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <!-- AdminLTE App -->
  <script src="{{ asset('AdminLTE/dist/js/adminlte.min.js') }}"></script>
  <script src="https://kit.fontawesome.com/6f41e4a92f.js" crossorigin="anonymous"></script>
</body>
</html>
