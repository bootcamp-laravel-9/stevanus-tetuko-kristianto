@extends('layout.main')
{{-- section ('('nama yield', 'valuenya')') --}}
@section('menu-title', 'Detail Member')
@section('menu-bootcamp', 'active')
@section('content')

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <div>
        <form action="{{ url('/edit-member-proses/') }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="card-body">
                <div class="form-group">
                    <input name="memberId" type="hidden" class="form-control" id="exampleInputEmail1" value="{{ $detail->id }}" {{ $action == 'show'?'disabled':'' }}>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Name</label>
                    <input name="memberName" type="text" class="form-control" id="exampleInputEmail1" value="{{ $detail->name }}" {{ $action == 'show'?'disabled':'' }}>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Universitas</label>
                    <input name="memberUniv" type="text" class="form-control" id="exampleInputEmail1" value="{{ $detail->univ }}"  {{ $action == 'show'?'disabled':'' }}>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Asal</label>
                    <input name="memberAsal" type="text" class="form-control" id="exampleInputEmail1" value="{{ $detail->asal }}"  {{ $action == 'show'?'disabled':'' }}>
                </div>
                @if($action!='show')
                    <div class="card">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                @endif
            </div>
        </form>
    </div>
</body>
</html>
@endsection