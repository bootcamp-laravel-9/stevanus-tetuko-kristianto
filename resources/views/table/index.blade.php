@extends('layout.main')
{{-- section ('('nama yield', 'valuenya')') --}}
@section('menu-title', 'Bootcamp Batch 9')
@section('menu-bootcamp', 'active')
@section('content')
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Table</title>
</head>
<body>
    <div>
        <div class="card">
            <div class="card-header">
            <h3 class="card-title">Bootcamp Batch 9</h3>
        </div>
    
        <div class="card-body">
            <table class="table table-hover text-nowrap">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Asal Universitas</th>
                    <th>Asal Daerah</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach($data as $index => $row)
                        <tr>
                            
                            <td>{{ $index + 1 }}</td>
                            <td>{{ $row['name'] }}</td>

                            @if($row['univ'] == 'UAJY')
                                <td style="color: rgb(0, 230, 11)">{{ $row['univ'] }}</td>
                            @elseif($row['univ'] == 'UPN')
                                <td style="color: red">{{ $row['univ'] }}</td>
                            @else
                                <td style="color: blue">{{ $row['univ'] }}</td>
                            @endif

                            {{-- <td>{{ $row['univ'] }}</td> --}}
                            <td>{{ $row['asal'] }}</td>
                            <td>
                                <a href="{{ url('/member-detail/'.$row->id) }}" class="btn btn-info">Detail</a>
                                <a href="{{ url('/member-edit/'.$row->id) }}" class="btn btn-success">Edit</a>
                                <a href="{{ url('/member-delete/'.$row->id) }}" class="btn btn-danger">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            {{-- <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nama</th>
                        <th>Asal Kampus</th>
                        <th>Asal Daerah</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1.</td>
                        <td>Christo</td>
                        <td>UAJY</td>
                        <td>Tegal</td>
                    </tr>
                    <tr>
                        <td>2.</td>
                        <td>Evan</td>
                        <td>UAJY</td>
                        <td>Solo</td>
                    </tr>
                    <tr>
                        <td>3.</td>
                        <td>Fathur</td>
                        <td>UPN</td>
                        <td>Jogja</td>
                    </tr>
                    <tr>
                        <td>4.</td>
                        <td>Samuel</td>
                        <td>UKDW</td>
                        <td>Purworejo</td>
                    </tr>
                    <tr>
                        <td>5.</td>
                        <td>Ryzal</td>
                        <td>UPN</td>
                        <td>Sumedang</td>
                    </tr>
                    <tr>
                        <td>6.</td>
                        <td>Edwin</td>
                        <td>UKDW</td>
                        <td>Jogja</td>
                    </tr>
                    <tr>
                        <td>7.</td>
                        <td>Kiky</td>
                        <td>UKDW</td>
                        <td>Jogja</td>
                    </tr>
                    <tr>
                        <td>8.</td>
                        <td>Nuel</td>
                        <td>UKDW</td>
                        <td>Tegal</td>
                    </tr>
                    <tr>
                        <td>9.</td>
                        <td>Bagas</td>
                        <td>UAJY</td>
                        <td>Kalimantan</td>
                    </tr>
                    <tr>
                        <td>10.</td>
                        <td>Alui</td>
                        <td>UAJY</td>
                        <td>Sumatra Selatan</td>
                    </tr>    
                </tbody>
            </table> --}}
        </div>
    </div>

</body>
</html>

@endsection