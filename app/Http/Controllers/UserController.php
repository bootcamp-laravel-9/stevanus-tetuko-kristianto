<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Member;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $data=[
        //     ['nama' => 'Christo', 'univ' => 'UAJY', 'asal' => 'Tegal'],
        //     ['nama' => 'Evan', 'univ' => 'UAJY', 'asal' => 'Solo'],
        //     ['nama' => 'Fathur', 'univ' => 'UPN', 'asal' => 'Jogja'],
        //     ['nama' => 'Samuel', 'univ' => 'UKDW', 'asal' => 'Purworejo'],
        //     ['nama' => 'Ryzal', 'univ' => 'UPN', 'asal' => 'Sumedang'],
        //     ['nama' => 'Edwin', 'univ' => 'UKDW', 'asal' => 'Jogja'],
        //     ['nama' => 'Kiky', 'univ' => 'UKDW', 'asal' => 'Jogja'],
        //     ['nama' => 'Nuel', 'univ' => 'UKDW', 'asal' => 'Jogja'],
        //     ['nama' => 'Bagas', 'univ' => 'UAJY', 'asal' => 'Kalimantan'],
        //     ['nama' => 'Alui', 'univ' => 'UAJY', 'asal' => 'Sumatra Selatan'],
        // ];

        //sekarang datanya ambil dari Member, bukan dari $data lagi
        $data = Member::all();
        return view('table/index', [
            'data' => $data
        ]);
        // return view('table.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $detail = Member::find($id);
        $action = 'show';
        return view('table/menu-detail', compact('detail', 'action'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $detail = Member::find($id);
        $action = 'edit';
        return view('table/menu-detail', compact('detail','action'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Member $member)
    {
        $updatemember = Member::find($request->memberId);

        $updatemember->id = $request->memberId;
        $updatemember->name= $request->memberName;
        $updatemember->univ= $request->memberUniv;
        $updatemember->asal = $request->memberAsal;

        if($updatemember->save()){
            return redirect('/table');
        }
        else{
            return redirect()->back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $member = Member::destroy($id);
        return redirect('/table');
    }
}
