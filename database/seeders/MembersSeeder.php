<?php

namespace Database\Seeders;

use App\Models\Member;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MembersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $members = [
            ['name' => 'Christo', 'univ' => 'UAJY', 'asal' => 'Tegal'],
            ['name' => 'Evan', 'univ' => 'UAJY', 'asal' => 'Solo'],
            ['name' => 'Fathur', 'univ' => 'UPN', 'asal' => 'Jogja'],
            ['name' => 'Samuel', 'univ' => 'UKDW', 'asal' => 'Purworejo'],
            ['name' => 'Ryzal', 'univ' => 'UPN', 'asal' => 'Sumedang'],
            ['name' => 'Edwin', 'univ' => 'UKDW', 'asal' => 'Jogja'],
            ['name' => 'Kiky', 'univ' => 'UKDW', 'asal' => 'Jogja'],
            ['name' => 'Nuel', 'univ' => 'UKDW', 'asal' => 'Jogja'],
            ['name' => 'Bagas', 'univ' => 'UAJY', 'asal' => 'Kalimantan'],
            ['name' => 'Alui', 'univ' => 'UAJY', 'asal' => 'Sumatra Selatan'],
        ];

        DB::beginTransaction();

        foreach($members as $member) {
            Member::firstOrCreate($member);
        }

        DB::commit();
    }
}
