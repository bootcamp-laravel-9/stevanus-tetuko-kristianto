<?php

use App\Http\Controllers\CvController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/', function () {
    return view('dashboard.index');
});

// Route::get('/table', function () {
//     return view('table.index');
// });

// Route::get('/table', [UserController::class, 'index']);

Route::get('/table', 'UserController@index');

Route::get('/member-detail/{id}', 'UserController@show');
Route::get('/member-edit/{id}', 'UserController@edit');
Route::put('/edit-member-proses', 'UserController@update');
Route::get('/member-delete/{id}', 'UserController@destroy');

Route::get('/cv', 'CvController@index');
Route::get('/login', 'AuthController@login');
Route::get('/register', 'AuthController@register');